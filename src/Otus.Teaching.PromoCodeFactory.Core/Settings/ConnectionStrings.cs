﻿namespace Otus.Teaching.PromoCodeFactory.Core.Settings
{
    /// <summary>
    /// Настройки со строками подключения к БД
    /// </summary>
    public class ConnectionStrings
    {
        /// <summary>
        /// Подключение к SqlLite
        /// </summary>
        public string ConnectionStringSqlLite { get; set; }
    }
}